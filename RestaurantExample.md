# 概述

假设有一个快餐小店，围绕小店可以形成一个小游戏。

游戏有如下角色：

老板：支付员工工资。

厨师：生产快餐。

外卖小哥：送外卖给客户。

客户：购买快餐。

游戏开始，用户注册成为客户（目前不需要实名），每个客户初始账户有100员。每个快餐10元。

厨师由老板选定。

外卖小哥每单挣1元。



# 1、定义订单
订单是系统的核心，客户、厨师、老板、送货小哥都需要和订单打交道。
```
define[订单]   //简单化，假设一个订单只能订一份，那么可以暂时不需要数量
//whether[defined[订单] ]
```
## 1.1、订单编号
```
define[订单编号, Link:Integer]  
whether[defined[Link:订单编号]]
```

## 2.1、订单客户
```
define[客户, Link:String]
```
## 2.2、订什么
```
define[品种, Link:String]
whether[defined[Link:品种]]
```
## 2.3、产品数量
```
define[数量, Link:Integer]
whether[defined[Link:数量]]
```
## 2.4、付款状态
```
define[payed, State]
whether[defined[State:payed]]
```
用一个payed状态记录是否付款。
## 2.5、发货状态
```
define[已发货, State]
whether[defined[State:已发货]]
```
## 2.6、结束状态
```
define[已结单, State]
whether[defined[State:已结单]]
```
## 2.7、下单时间
```
define[下单时间, Link:String]
whether[defined[Link:下单时间]]
```
# 2、产生订单
产生两个订单，编号分别为2000，2001
```
create[订单{订单编号:=2000,品种:="木桶饭", 数量:=2, 下单时间:=now.toString, payed::false}]
create[订单{订单编号:=2001,品种:="盖浇饭", 数量:=1, 下单时间:=now.toString, payed::false}]
```
## 查看所有订单
```
ask[订单.all]
```
## 根据订单号选择订单
```
ask[订单.select{订单编号=2000}]
```
## 改变订单状态
```
订单.select{订单编号=2000}#turn[payed, true]
whether[订单.select{订单编号=2000}$payed]
whether[订单.select{订单编号=2000}!payed]
```
## 根据付款状态查看订单
```
ask[订单.select{payed}]   //已付款订单
ask[订单.select{!payed}]  //未付款订单
```
# 3、产生订单编号
通过新建一个对象，对象中设计一个变量来保持当前编号，最新编号总是在当前编号+1。
```
define[编号生成器,Object:create[Object]]
define[lastSeq, Link:Integer]
define[nextSeq, Link:Integer]
编号生成器#assign[lastSeq,0]
//ask[编号生成器.lastSeq]
编号生成器#declare[Calculation:nextSeq,{assign[lastSeq, lastSeq+1],return[lastSeq]}] ask[编号生成器.nextSeq]   //1
ask[编号生成器.nextSeq]   //2，每次加1
```

# 3、权限管理
注册用户,用户名要求必须是id格式。
```
register("Rose", "111111")
hasUser("Rose")  //whether[hasUser("Rose")]
register("Jack", "111111")
```
3.1 用户登录
```
login[Rose,"111111"]
login[Jack,"111111"]
ask[me]
ask[me.userid]
logout
```
# 4、下订单
```
//下单的前提是客户已登录
define[newOrder(String, Integer), Action]
declare[Action:newOrder(String x, Integer y) ,create[订单{订单编号:=编号生成器.nextSeq,品种:=x, 数量:=y, 客户:=me.userid,下单时间:=now.toString, payed::false}] ]
newOrder("盖浇饭", 3)
```
## 4.1、我的订单
```
ask[订单.select{订单编号=4, 客户=me.userid}]
```
# 5、客户支付
```
//前提：客户已登录
define[pay(Integer), Action]
declare[Action:pay(Integer x), 订单.select{订单编号=x}#turn[payed, true]]
```
# 6、客户支付
```
define[send(Integer), Action]
declare[Action:send(Integer x), 订单.select{订单编号=x}#turn[已发货, true]]
```
# 7、结单
```
define[deal(Integer), Action]
declare[Action:deal(Integer x), 订单.select{订单编号=x}#turn[已结单, true]]
ask[订单.select{订单编号=3}]
```