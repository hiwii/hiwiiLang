package net.hiwii.obj.file;

import net.hiwii.view.HObject;

public class MappingObject extends HObject {
	private HObject target;

	public HObject getTarget() {
		return target;
	}

	public void setTarget(HObject target) {
		this.target = target;
	}
}
