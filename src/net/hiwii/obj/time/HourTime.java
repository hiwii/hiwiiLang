package net.hiwii.obj.time;

/**
 * 格式："日期HH"
 * @author hiwii
 *
 */
public class HourTime extends DateTime {
	private String hour;

	public String getHour() {
		return hour;
	}

	public void setHour(String hour) {
		this.hour = hour;
	}
}
