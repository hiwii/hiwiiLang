package net.hiwii.obj.time;

import java.util.Calendar;

import net.hiwii.view.HObject;

public class TimeObject extends HObject {
	private String time;
	
	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public void init(Calendar cal) {}
}
