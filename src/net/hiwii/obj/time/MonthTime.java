package net.hiwii.obj.time;

public class MonthTime extends YearTime {
	private String month;

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}
}
