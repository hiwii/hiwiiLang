package net.hiwii.obj.time;

/**
 * 用4位整数表示年。前面可以有负号。
 * 当时间前有负号，表示公元前。公元后年代不能有符号。
 * 当时间不够四位，前面需要补0，如："0950" "0049" "0003" "0000" "-0036"
 * 年代的表示：
 * 当需要表示世纪(Century), Year的首字符是C，公元前的负号在C之前。世纪只需要2位数字，如：
 * "C19"(19公元前5世纪) "-C05"(公元前5世纪)
 * 当需要表示年代，数字必须是两位10倍整数，如下：
 * "D1960" 19世纪60年代，"-D0560" 公元前5世纪60年代，
 * 
 * @author hiwii
 *
 */
public class YearTime extends TimeObject {
	private String year;

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@Override
	public String toString() {
		return year;
	}
	
}
