package net.hiwii.obj.time;

import java.util.Calendar;

/**
 * 格式："日期"
 * @author hiwii
 *
 */
public class DateTime extends MonthTime {
	private String date;
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public void init(Calendar cal) {
		setYear(String.valueOf(cal.get(Calendar.YEAR)));
		setMonth(String.format("%02d", cal.get(Calendar.MONTH)+1));
		setDate(String.format("%02d", cal.get(Calendar.DATE)));
	}
	
	@Override
	public String toString() {
		return getYear() + getMonth() + getDate();
	}
}
