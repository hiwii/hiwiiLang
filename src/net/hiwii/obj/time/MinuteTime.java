package net.hiwii.obj.time;

/**
 * 格式："日期HHmm"
 * @author hiwii
 *
 */
public class MinuteTime extends HourTime {
	private String minute;

	public String getMinute() {
		return minute;
	}

	public void setMinute(String minute) {
		this.minute = minute;
	}
}
