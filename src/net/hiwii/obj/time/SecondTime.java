package net.hiwii.obj.time;

/**
 * 格式："日期HHmmss"
 * @author hiwii
 *
 */
public class SecondTime extends MinuteTime {
	private String second;

	public String getSecond() {
		return second;
	}

	public void setSecond(String second) {
		this.second = second;
	}
}
