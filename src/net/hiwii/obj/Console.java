package net.hiwii.obj;

import net.hiwii.system.LocalHost;
import net.hiwii.system.exception.ApplicationException;

public class Console {
	public static void main(String[] args) {
		try {
			if(args.length == 0) {
				LocalHost host = LocalHost.getInstance();
				host.startHttpServer();
				host.startup();
			}else if(args.length == 2) {
				if(args[0].equals("-p")) {   //option -p
					String arg1 = args[1];
					if(arg1.matches("[0-9]+")) { 
						int port = Integer.parseInt(arg1);
						LocalHost host = LocalHost.getInstance();
						host.startHttpServer(port);
						host.startup();
					}
				}else {
					throw new ApplicationException("command line option error: -p 8080!");
				}				
			}else {
				throw new ApplicationException("command line option error: -p 8080!");
			}
		} catch (ApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
