package net.hiwii.entity;

import net.hiwii.cognition.Expression;
import net.hiwii.view.HObject;

/**
 * 
 * @author Administrator
 * 一个表达式，用于包裹entity。
 *
 */
public class EntityWrapper extends Expression {
	private HObject content;
	public HObject getContent() {
		return content;
	}
	public void setContent(HObject content) {
		this.content = content;
	}
	@Override
	public String toString() {
		return "a(" + content.toString() + ")";
	}
}
