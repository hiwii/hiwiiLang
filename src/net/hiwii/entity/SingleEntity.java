package net.hiwii.entity;

import net.hiwii.view.HObject;

/**
 * a(definition)函数，日常中的一个对象，英语中的 a rabbit
 * @author Administrator
 *
 */
public class SingleEntity extends HObject {
	private HObject target;

	public HObject getTarget() {
		return target;
	}
	public void setTarget(HObject target) {
		this.target = target;
	}

	@Override
	public String toString() {
		return "a(" + target.toString() + ")";
	}
}
