package net.hiwii.entity;

import net.hiwii.view.HObject;

public class ImageEntity extends HObject {
	private String path;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}
