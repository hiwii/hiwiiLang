package net.hiwii.cognition.result;

import net.hiwii.cognition.Expression;
import net.hiwii.view.HObject;

public class ReturnResult extends Expression{
	private HObject result;

	public HObject getResult() {
		return result;
	}

	public void setResult(HObject result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return result.toString();
	}
}
