package net.hiwii.cognition.result;

import net.hiwii.message.HiwiiException;
import net.hiwii.view.HObject;

public class ExceptionObject extends HObject {
	private HiwiiException exception;

	public HiwiiException getException() {
		return exception;
	}
	public void setException(HiwiiException exception) {
		this.exception = exception;
	}
	@Override
	public String toString() {
		return exception.toString();
	}
}
