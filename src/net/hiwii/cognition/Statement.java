package net.hiwii.cognition;

public class Statement extends Expression{
	private long stateTime;
	private boolean positive;
//	private TimeObject verbTime;
//	private String progress;  //一般do，进行doing，完成done，完成进行been
//	private String happen;  //现在be，过去was，将来will，过去未来would
//	private Expression sentence;
	

	public long getStateTime() {
		return stateTime;
	}

	public void setStateTime(long stateTime) {
		this.stateTime = stateTime;
	}
	public boolean isPositive() {
		return positive;
	}

	public void setPositive(boolean positive) {
		this.positive = positive;
	}


//	public String getProgress() {
//		return progress;
//	}
//
//	public void setProgress(String progress) {
//		this.progress = progress;
//	}
//
//	public String getHappen() {
//		return happen;
//	}
//
//	public void setHappen(String happen) {
//		this.happen = happen;
//	}

	

}
