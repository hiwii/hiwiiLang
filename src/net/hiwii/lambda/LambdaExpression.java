package net.hiwii.lambda;

import java.util.List;

import net.hiwii.cognition.Expression;

/**
 * 与java、scala等lambda表达式类似，可以采用=>操作符表示lambda运算。
 * x=>5   //恒等于一个常数
 * (x,y)=>(x+y)  //两个参数
 * x=>{int i=3, x:=x+i, return[x]}  //程序作为表达式
 * @author hiwii
 *
 */
public class LambdaExpression extends Expression {
	private List<String> keys;
	private Expression statement;
	
	public List<String> getKeys() {
		return keys;
	}
	public void setKeys(List<String> keys) {
		this.keys = keys;
	}
	public Expression getStatement() {
		return statement;
	}
	public void setStatement(Expression statement) {
		this.statement = statement;
	}
	
	@Override
	public String toString() {
		String ret = "TODO";
		return ret;
	}
}
