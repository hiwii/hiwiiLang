package net.hiwii.state;

import java.util.List;

import net.hiwii.cognition.Expression;
import net.hiwii.cognition.Statement;

public class MappingStatement extends Statement {
	private String name;
	private List<Expression> arguments;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Expression> getArguments() {
		return arguments;
	}
	public void setArguments(List<Expression> arguments) {
		this.arguments = arguments;
	}
	
}
