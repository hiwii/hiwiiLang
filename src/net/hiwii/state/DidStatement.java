package net.hiwii.state;

import java.util.List;

import net.hiwii.cognition.Expression;
import net.hiwii.cognition.Statement;

public class DidStatement extends Statement{
	private Expression verb;
	private List<Expression> adverb;
	public Expression getVerb() {
		return verb;
	}
	public void setVerb(Expression verb) {
		this.verb = verb;
	}
	public List<Expression> getAdverb() {
		return adverb;
	}
	public void setAdverb(List<Expression> adverb) {
		this.adverb = adverb;
	}
	
	
}
