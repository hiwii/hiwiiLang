package net.hiwii.state;

import net.hiwii.view.HObject;

/**
 * 当initial=null，初始状态未知
 * @author Administrator
 *
 */
public class ObjectState extends HObject {
	private String name;
//	private JudgmentResult initial;
	public ObjectState(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
//	public JudgmentResult getInitial() {
//		return initial;
//	}
//	public void setInitial(JudgmentResult initial) {
//		this.initial = initial;
//	}
	@Override
	public String toString() {
		return name;
	}
}
