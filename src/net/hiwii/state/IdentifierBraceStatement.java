package net.hiwii.state;

import java.util.List;

import net.hiwii.cognition.Expression;

public class IdentifierBraceStatement extends IdStatement {
//	private String name;
	private List<Expression> conditions;
//	public String getName() {
//		return name;
//	}
//	public void setName(String name) {
//		this.name = name;
//	}
	public List<Expression> getConditions() {
		return conditions;
	}
	public void setConditions(List<Expression> conditions) {
		this.conditions = conditions;
	}
}
