package net.hiwii.state;

import net.hiwii.view.HObject;

public class VirtualStatement extends HObject {
	private boolean judge;

	public boolean isJudge() {
		return judge;
	}
	public void setJudge(boolean judge) {
		this.judge = judge;
	}
}
