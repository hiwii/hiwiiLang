package net.hiwii.state.sub;

import net.hiwii.state.FunctionStatement;
import net.hiwii.view.HObject;

public class SubjectFunctionStatement extends FunctionStatement {
	private HObject subject;

	public HObject getSubject() {
		return subject;
	}

	public void setSubject(HObject subject) {
		this.subject = subject;
	}
	
}
