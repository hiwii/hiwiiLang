package net.hiwii.state.sub;

import net.hiwii.state.MappingStatement;
import net.hiwii.view.HObject;

public class SubjectMappingStatement extends MappingStatement {
	private HObject subject;

	public HObject getSubject() {
		return subject;
	}

	public void setSubject(HObject subject) {
		this.subject = subject;
	}
	
}
