package net.hiwii.state;

import net.hiwii.cognition.Expression;

public class BinaryLink extends LinkStatement {
	private Expression left;
	private Expression right;
	private String link;
	
	public Expression getLeft() {
		return left;
	}
	public void setLeft(Expression left) {
		this.left = left;
	}
	public Expression getRight() {
		return right;
	}
	public void setRight(Expression right) {
		this.right = right;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	
}
