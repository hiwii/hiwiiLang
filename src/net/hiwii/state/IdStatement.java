package net.hiwii.state;

import net.hiwii.cognition.Statement;

public class IdStatement extends Statement {
	private String name;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
