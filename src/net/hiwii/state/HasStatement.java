package net.hiwii.state;

import net.hiwii.cognition.Expression;
import net.hiwii.cognition.Statement;

/**
 * has[number, target]
 * number,target是expression。
 * number可以是Integer，some，
 * @author hiwii
 * LastUpdate 2022年6月28日
 */
public class HasStatement extends Statement {
	private String name;
//	private List<Expression> conditions;
	private Expression number;
	private Expression target;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Expression getNumber() {
		return number;
	}
	public void setNumber(Expression number) {
		this.number = number;
	}
	public Expression getTarget() {
		return target;
	}
	public void setTarget(Expression target) {
		this.target = target;
	}

	
}
