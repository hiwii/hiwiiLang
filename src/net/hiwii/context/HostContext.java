package net.hiwii.context;

import net.hiwii.cognition.Expression;
import net.hiwii.view.HObject;

public class HostContext extends RuntimeContext {
	private HObject host;

	public HostContext(HObject host) {
		this.host = host;
	}

	public HObject getHost() {
		return host;
	}

	public void setHost(HObject host) {
		this.host = host;
	}
	
	public Expression doAction(Expression expr){
		return host.doAction(expr);
	}
	
	public HObject doCalculation(Expression expr){
		return host.doCalculation(expr);
	}
}
