package net.hiwii.system.runtime;

import net.hiwii.view.HObject;

public class LiteralMap {
	private String name;
	private HObject literal;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public HObject getLiteral() {
		return literal;
	}
	public void setLiteral(HObject literal) {
		this.literal = literal;
	}
}
