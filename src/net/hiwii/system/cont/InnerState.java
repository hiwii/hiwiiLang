package net.hiwii.system.cont;

public enum InnerState{
	GT(">"),LT("<"),GE(">="),LE("<="),
	NE("!="),EQ("=");
	
	private final String value;

	InnerState(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return value;
	}
}