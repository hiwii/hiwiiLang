package net.hiwii.system.cont;

import java.util.Arrays;
import java.util.List;

public interface Constants {
	//string 存储时，为提高效率，超过maxstr长度的字符串将单独存储。
	int Maxstr = 140;
	int maxArgs = 1;
	List<String> ProgressList = Arrays.asList("do", "doing", "done");
	List<String> HappenList = Arrays.asList("be", "was", "will");
	List<String> SystemActions = Arrays.asList("exit", "assign", "turn");
	
	
}
