package net.hiwii.system.cont;

public enum EntityType{

	Entity("en"),Time("ti"),User("us"),ShortString("ss"),InstanceId("in"),
	LongString("ls"),LongBinary("lb"),ShortInteger("si"),ShortFloat("sf"),
	ShortFraction("sr"),ShortScnNumber("sn"),LongInteger("li"),LongFloat("lf"),
	LongFraction("lr"),LongScnNumber("ln");
	//Float和Fraction同以F开头，Fraction以rational代替。ScnNumber为避开String以n代替
	private final String value;

	EntityType(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}

}
