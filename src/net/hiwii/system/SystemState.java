package net.hiwii.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.hiwii.arg.Argument;
import net.hiwii.cognition.Expression;
import net.hiwii.db.ent.MappingFormat;

public class SystemState {
	private static List<String> idStates = new ArrayList<String>();
	private static Map<String, Expression> funStates = new HashMap<String, Expression>();
	private static Map<String, MappingFormat> mapStates = new HashMap<String, MappingFormat>();
	
	static{
		idStates.add("Red");
		MappingFormat mf1 = new MappingFormat();
		mf1.setName("did");
		List<Argument> arguments = new ArrayList<Argument>();
		Argument arg = new Argument();
		arg.setName("x");
		mf1.setArguments(arguments);
		mapStates.put("did#1", mf1);
	}
}
