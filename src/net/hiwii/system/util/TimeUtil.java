package net.hiwii.system.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import net.hiwii.obj.time.DateTime;
import net.hiwii.obj.time.YearTime;
import net.hiwii.system.exception.ApplicationException;

public class TimeUtil {
	public static final SimpleDateFormat yearfmt = new SimpleDateFormat("yyyy");
	public static final SimpleDateFormat monthfmt = new SimpleDateFormat("yyyyMM");
	public static final SimpleDateFormat datefmt = new SimpleDateFormat("yyyyMMdd");
	public static final SimpleDateFormat hourfmt = new SimpleDateFormat("yyyyMMdd HH");
	public static final SimpleDateFormat minitefmt = new SimpleDateFormat("yyyyMMdd HH:mm");
	public static final SimpleDateFormat secondfmt = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
	public static final SimpleDateFormat milliTimeFmt = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
	
	public static YearTime yearTime(){
		Calendar cal = Calendar.getInstance();
		YearTime time = new YearTime();
		time.setYear(String.valueOf(cal.get(Calendar.YEAR)));
		return time;
	}
	public static YearTime yearTime(String year){
		if(isYearTime(year) != null) {
			YearTime time = new YearTime();
			time.setYear(year);
			return time;
		}
		return null;
	}
	
	public static DateTime dateTime(){
		DateTime time = new DateTime();
		Calendar cal=Calendar.getInstance();  
		time.init(cal);
		return time;
	}
	public static DateTime dateTime(String str){
		Date date = isDateTime(str);
		if(date != null) {
			DateTime time = new DateTime();
			Calendar cal = Calendar.getInstance();  
		    cal.setTime(date); 
			time.init(cal);
			return time;
		}
		return null;
	}
	
	/**
	 * year最少4位，如果第一位是"-"表示公元前，最少5位。
	 * @param time
	 * @return
	 */
	public static Date isYearTime(String time) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
			Date date = sdf.parse(time);
			if(date != null) {
				return date;
			}
			return null;
		} catch (ParseException e) {
			return null;
		}
	}
	
	public static Date isMonthTime(String time) {
		try {
			Date date = monthfmt.parse(time);
			if(date != null) {
				return date;
			}
			return null;
		} catch (ParseException e) {
			return null;
		}
	}
	
	public static Date isDateTime(String time) {
		String time1 = time.replaceAll("[./-]", "");
		try {
			Date date = datefmt.parse(time1);
			if(date != null) {
				return date;
			}
			return null;
		} catch (ParseException e) {
			return null;
		}
	}
	
	/**
	 * Century 19
	 * @param time
	 * @return
	 */
	public static String getCentury(String time) throws ApplicationException {		
		if(time.length() < 4) {
			return null;
		}
		if(time.length() == 4 && time.substring(0,1).equals("-")) {
			return null;
		}
		String year = null;
		if(time.substring(0,1).equals("-")) {
			if(time.length() == 5) {
				year = time;
			}else {
				year = time.substring(0,5);
			}
		}else {
			if(time.length() == 4 ) {
				year = time;
			}else {
				year = time.substring(0,4);
			}
		}
		Date dd = isYearTime(year);
		return year;
	}
	
	/**
	 * 前面是时间参数可以更详细。当exp1在exp2内，返回true。
	 * 当后面参数更详细，不能确定exp1是否在exp2内，返回false。
	 * 以上设定是用于陈述和引用，引用可以比陈述的时间更精确，比如：一件事发生于1986年，则可以引用为"D1980"发生了这件事。
	 */	
	public static boolean compareTime(String time1, String time2) 
			throws ApplicationException {
		if(time1.length() == time2.length()) {
			if(time1.equals(time2)) {
				return true;
			}
			return false;
		}
		char ch1 = time1.charAt(0);
		char ch2 = time2.charAt(0);
		if(time1.length() > time2.length()) {
			if(ch1 == '-' && ch2 == '-') {
				return compareTimePlus(time1.substring(1), time2.substring(1));
			}else if((ch1 == '-' && ch1 != ch2) || (ch2 == '-' && ch1 != ch2)) {
				return false;
			}else {
				return compareTimePlus(time1, time2);
			}
		}
		return false;
	}
	
	public static boolean compareTimePlus(String time1, String time2) 
			throws ApplicationException {
		if(time1.length() == time2.length()) {
			if(time1.equals(time2)) {
				return true;
			}
			return false;
		}
		char ch1 = time1.charAt(0);
		char ch2 = time2.charAt(0);
		if(ch1 == 'C') {
			if(!(time1.length() == 3)) {
				throw new ApplicationException();
			}
			String cen1 = time1.substring(1, 3);
			if(ch2 == 'C') {
				if(time2.length() == 3) {
					throw new ApplicationException();
				}
				String cen2 = time2.substring(1, 3);
				if(cen1.equals(cen2)) {
					return true;
				}
				return false;
			}else if(ch2 == 'D') {
				throw new ApplicationException();
			}else {
				throw new ApplicationException();
			}
		}else if(ch1 == 'D') {
			if(!(time1.length() == 5)) {
				throw new ApplicationException();
			}
			String dec1 = time1.substring(1, 3);
			if(ch2 == 'C') {
				if(time2.length() == 3) {
					if(time1.substring(1, 3).equals(time2.substring(1, 3))) {
						return true;
					}
					return false;
				}
				throw new ApplicationException("unable to judge!");
			}else if(ch2 == 'D') {
				if(!(time2.length() == 5)) {
					throw new ApplicationException();
				}
				String dec2 = time2.substring(1, 5);
				if(dec1.equals(dec2)) {
					return true;
				}
				return false;
			}else {
				//yyyy or yyyy-mm-dd...
				if(time2.length() < 4) {
					throw new ApplicationException();
				}
				String dec2 = time2.substring(0, 4);
				if(dec1.equals(dec2)) {
					return true;
				}
				return false;
			}
		}else if(time1.length() == 4) {
			String year1 = time1.substring(0, 4);
			if(ch2 == 'C') {
				if(time2.length() == 3) {
					if(year1.substring(0, 2).equals(time2.substring(1, 3))) {
						return true;
					}
					return false;
				}
				throw new ApplicationException("unable to judge!");
			}else if(ch2 == 'D') {
				if(time2.length() == 5) {
					if(year1.substring(0, 3).equals(time2.substring(1, 4))) {
						return true;
					}
					return false;
				}
				throw new ApplicationException("unable to judge!");
			}else {
				if(time2.length() < 4) {
					throw new ApplicationException();
				}
				String year2 = time2.substring(0, 4);
				if(time2.length() == 4 ) {
					if(year1.equals(year2)) {
						return true;
					}
					return false;
				}
				throw new ApplicationException();			
			}
		}else if(time1.length() == 6) {
			String year1 = time1.substring(0, 4);
			String mon1 = time1.substring(4, 6);
			if(ch2 == 'C') {
				if(time2.length() == 3) {
					if(year1.substring(0, 2).equals(time2.substring(1, 3))) {
						return true;
					}
					return false;
				}
				throw new ApplicationException("unable to judge!");
			}else if(ch2 == 'D') {
				if(time2.length() == 5) {
					if(year1.substring(0, 3).equals(time2.substring(1, 4))) {
						return true;
					}
					return false;
				}
				throw new ApplicationException("unable to judge!");
			}else {
				if(time2.length() < 4) {
					throw new ApplicationException();
				}
				String year2 = time2.substring(0, 4);
				if(time2.length() == 4) {
					if(year1.equals(year2)) {
						return true;
					}
					return false;
				}				
				if(time2.length() == 6) {
					String mon2 = time2.substring(4, 6);
					if(year1.equals(year2) && mon1.equals(mon2)) {
						return true;
					}
					return false;
				}
				throw new ApplicationException();			
			}
		}else if(time1.length() == 8) {
			String year1 = time1.substring(0, 4);
			String mon1 = time1.substring(4, 6);
			String day1 = time1.substring(6, 8);
			if(ch2 == 'C') {
				if(time2.length() == 3) {
					if(year1.substring(0, 2).equals(time2.substring(1, 3))) {
						return true;
					}
					return false;
				}
				throw new ApplicationException("unable to judge!");
			}else if(ch2 == 'D') {
				if(time2.length() == 5) {
					if(year1.substring(0, 3).equals(time2.substring(1, 4))) {
						return true;
					}
					return false;
				}
				throw new ApplicationException("unable to judge!");
			}else {
				if(time2.length() < 4) {
					throw new ApplicationException();
				}
				String year2 = time2.substring(0, 4);
				if(time2.length() == 4) {
					if(year1.equals(year2)) {
						return true;
					}
					return false;
				}
				if(time2.length() == 6) {
					String mon2 = time2.substring(4, 6);
					if(year1.equals(year2) && mon1.equals(mon2)) {
						return true;
					}
					return false;
				}				
				if(time2.length() == 8) {
					String mon2 = time2.substring(4, 6);
					String day2 = time2.substring(6, 8);
					if(year1.equals(year2) && mon1.equals(mon2) && day1.equals(day2)) {
						return true;
					}
					return false;
				}
				throw new ApplicationException();			
			}
		}else if(time1.length() == 10) {
			String year1 = time1.substring(0, 4);
			String mon1 = time1.substring(4, 6);
			String day1 = time1.substring(6, 8);
			String hour1 = time1.substring(8, 10);
			if(ch2 == 'C') {
				if(time2.length() == 3) {
					if(year1.substring(0, 2).equals(time2.substring(1, 3))) {
						return true;
					}
					return false;
				}
				throw new ApplicationException("unable to judge!");
			}else if(ch2 == 'D') {
				if(time2.length() == 5) {
					if(year1.substring(0, 3).equals(time2.substring(1, 4))) {
						return true;
					}
					return false;
				}
				throw new ApplicationException("unable to judge!");
			}else {
				if(time2.length() < 4) {
					throw new ApplicationException();
				}
				String year2 = time2.substring(0, 4);
				if(time2.length() == 4) {
					if(year1.equals(year2)) {
						return true;
					}
					return false;
				}else if(time2.length() == 6) {
					String mon2 = time2.substring(4, 6);
					if(year1.equals(year2) && mon1.equals(mon2)) {
						return true;
					}
					return false;
				}else if(time2.length() == 8) {
					String mon2 = time2.substring(4, 6);
					String day2 = time2.substring(6, 8);
					if(year1.equals(year2) && mon1.equals(mon2) && day1.equals(day2)) {
						return true;
					}
					return false;
				}else if(time2.length() == 10) {
					String mon2 = time2.substring(4, 6);
					String day2 = time2.substring(6, 8);
					String hour2 = time2.substring(8, 10);
					if(year1.equals(year2) && mon1.equals(mon2) && day1.equals(day2)) {
						return true;
					}
					return false;
				}
				throw new ApplicationException();			
			}
		}
		return false;
	}
	
	public static boolean verifyDateLegal(String date) {
        if ((date.contains("-") && date.contains("/"))
                || (date.contains("-") && date.contains("."))
                || (date.contains("/") && date.contains("."))){
            return false;
        }
        date.trim();
        StringBuilder timeSb = new StringBuilder();
        date = date.replaceAll("[./]", "-");
        String[] time = date.split(" ");
        timeSb.append(time[0]);
        timeSb.append(" ");
        if (time.length > 1) {
            timeSb.append(time[1]);
        }
        int i = time.length > 1 ? time[1].length() : 0;
        for ( ; i < 8 ; i ++) {
            if (i == 2 || i == 5){
                timeSb.append(":");
            } else {
                timeSb.append("0");
            }
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            sdf.setLenient(false);
            sdf.parse(timeSb.toString());
            return true;
        } catch (ParseException e) {
            return false;
        }
    }
}
