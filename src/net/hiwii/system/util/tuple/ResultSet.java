package net.hiwii.system.util.tuple;

import java.util.ArrayList;
import java.util.List;

import net.hiwii.view.HObject;

public class ResultSet extends HObject {
	private List<HObject> values;

	public ResultSet() {
		values = new ArrayList<HObject>();
	}

	public List<HObject> getValues() {
		return values;
	}

	public void setValues(List<HObject> values) {
		this.values = values;
	}
	
	public void putValue(HObject value){
		values.add(value);
	}
	
	public HObject getValue(){
		if(values.size() == 1){
			return values.get(0);
		}else{
			return null;
		}
	}
	
	public HObject getValue(int n){
		if(values.size() <= n && n > 0){
			return values.get(n + 1);
		}else{
			return null;
		}
	}
}
