package net.hiwii.system;

import java.util.HashMap;
import java.util.Map;

import net.hiwii.system.exception.ApplicationException;
import net.hiwii.system.util.tuple.TwoTuple;

public class SystemOperators {
	public static Map<String, TwoTuple<String, Integer>> allop = 
			new HashMap<String, TwoTuple<String, Integer>>();
	public static Map<String, TwoTuple<String, Integer>> funop = 
		new HashMap<String, TwoTuple<String, Integer>>();
	
	
	static{
		allop.put("^", new TwoTuple<String, Integer>("pow", 60));
//		allop.put("!", new TwoTuple<String, Integer>("not", 60));
		
		allop.put("@", new TwoTuple<String, Integer>("AT", 60));
		allop.put("~", new TwoTuple<String, Integer>("tilde", 60));
		allop.put("`", new TwoTuple<String, Integer>("FENG", 60));
		allop.put("&&", new TwoTuple<String, Integer>("SCand", 60));
		allop.put("||", new TwoTuple<String, Integer>("SCor", 60));
		allop.put(">>", new TwoTuple<String, Integer>("RSHIFT", 60));
		allop.put("<<", new TwoTuple<String, Integer>("LSHIFT", 60));
				
		allop.put("*", new TwoTuple<String, Integer>("multiply", 50));
		allop.put("%", new TwoTuple<String, Integer>("Percent", 50));
		allop.put("/", new TwoTuple<String, Integer>("divide", 50));
		
		allop.put("+", new TwoTuple<String, Integer>("plus", 40));
		allop.put("-", new TwoTuple<String, Integer>("minus", 40));
		
		allop.put("<>", new TwoTuple<String, Integer>("LTGT", 30));
		allop.put(">", new TwoTuple<String, Integer>("GT", 30));
		allop.put("<", new TwoTuple<String, Integer>("LT", 30));
		allop.put(">=", new TwoTuple<String, Integer>("GE", 30));
		allop.put("<=", new TwoTuple<String, Integer>("LE", 30));
		
//		allop.put("=>", new TwoTuple<String, Integer>("lambda", 30));
		
		allop.put("->", new TwoTuple<String, Integer>("belong", 25));
		allop.put("<-", new TwoTuple<String, Integer>("contain", 25));
		
		allop.put("::", new TwoTuple<String, Integer>("turn", 20));
		allop.put("==", new TwoTuple<String, Integer>("LEQ", 20));
		
		allop.put("!=", new TwoTuple<String, Integer>("NE", 20));
		
		allop.put("&", new TwoTuple<String, Integer>("and", 10));
		allop.put("|", new TwoTuple<String, Integer>("or", 0));
		
		allop.put("?", new TwoTuple<String, Integer>("HOOK", -10));
		allop.put("=", new TwoTuple<String, Integer>("EQ", -10));
		allop.put(":=", new TwoTuple<String, Integer>("assign", -10));
					
		allop.put(":", new TwoTuple<String, Integer>("describe", -20));
		
		//作为内部语句分隔符
		allop.put(";", new TwoTuple<String, Integer>("SEMI", -30));
		//lambda定义， lambda 操作符被绑定到它后面的整个表达式。应用@，左结合
		allop.put("=>", new TwoTuple<String, Integer>("lambda", -40));
		allop.put("", new TwoTuple<String, Integer>("Space", -100));
		
		funop.put("^", new TwoTuple<String, Integer>("pow", 60));
		funop.put("*", new TwoTuple<String, Integer>("multiply", 50));
		funop.put("%", new TwoTuple<String, Integer>("Percent", 50));
		funop.put("/", new TwoTuple<String, Integer>("divide", 50));
		
		funop.put("+", new TwoTuple<String, Integer>("plus", 40));
		funop.put("-", new TwoTuple<String, Integer>("minus", 40));
		
		funop.put("<>", new TwoTuple<String, Integer>("LTGT", 30));
		funop.put(">", new TwoTuple<String, Integer>("GT", 30));
		funop.put("<", new TwoTuple<String, Integer>("LT", 30));
		funop.put(">=", new TwoTuple<String, Integer>("GE", 30));
		funop.put("<=", new TwoTuple<String, Integer>("LE", 30));
	}
	
	public static boolean isOperator(String op){
		if (allop.containsKey(op)){
			return true;
		}else{
			return false;
		}
	}
	
	public static boolean isFunOperator(String op){
		if (funop.containsKey(op)){
			return true;
		}else{
			return false;
		}
	}
	
	public static int compare(String op1, String op2){
		return allop.get(op1).getB() - allop.get(op2).getB();
	}
	
	public static String getOperationName(String op) throws ApplicationException{
		if (allop.containsKey(op)){
			return allop.get(op).getA();
		}else{
			throw new ApplicationException("not operator!");
		}
	}
}
