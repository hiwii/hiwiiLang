package net.hiwii.system;

import java.util.ArrayList;
import java.util.List;

import net.hiwii.view.HObject;

public class SystemAdjective extends HObject {
	public static List<String> array = new ArrayList<String>();
		
	static{
		array.add("precise");
	}
	
	public static boolean contains(String str){
		return array.contains(str);
	}
}
