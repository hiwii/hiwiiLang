package net.hiwii.system.info;

import java.util.List;

import net.hiwii.view.HObject;

public class FunctionPropertyInfo extends HObject {
	private List<HObject> args;
	private HObject value;
	public List<HObject> getArgs() {
		return args;
	}
	public void setArgs(List<HObject> args) {
		this.args = args;
	}
	public HObject getValue() {
		return value;
	}
	public void setValue(HObject value) {
		this.value = value;
	}
}
