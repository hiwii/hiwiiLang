package net.hiwii.system.obj;

import java.io.IOException;

import com.sleepycat.je.DatabaseException;

import net.hiwii.cognition.Expression;
import net.hiwii.cognition.result.NormalEnd;
import net.hiwii.db.HiwiiDB;
import net.hiwii.message.HiwiiException;
import net.hiwii.system.LocalHost;
import net.hiwii.system.exception.ApplicationException;
import net.hiwii.view.HObject;

public class IdentifierDomain extends HObject {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Expression doDescribe(Expression expr) {
		HiwiiDB db = LocalHost.getInstance().getHiwiiDB();
		try {
			db.setIdDomain(name, expr.toString(), null);
			return new NormalEnd();
		} catch (DatabaseException e) {
			return new HiwiiException();
		} catch (IOException e) {
			return new HiwiiException();
		} catch (ApplicationException e) {
			return new HiwiiException();
		} catch (Exception e) {
			return new HiwiiException();
		}
//		return null;
	}

	@Override
	public String toString() {
		String str = "Domain" + "." + name;
		return str;
	}
	
}
