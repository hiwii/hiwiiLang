package net.hiwii.system.obj;

import java.io.IOException;
import java.util.List;

import com.sleepycat.je.DatabaseException;

import net.hiwii.def.Definition;
import net.hiwii.message.HiwiiException;
import net.hiwii.prop.Link;
import net.hiwii.system.exception.ApplicationException;
import net.hiwii.system.util.EntityUtil;
import net.hiwii.view.HObject;

public class AbstractDomain extends Definition {
	
	public AbstractDomain() {
		super();
		setClassName("Domain");
		setSignature("L.D");
	}

	@Override
	public HObject doIdentifierCalculation(String name) {
		try {
			Link prop = EntityUtil.proxyGetProperty(name);
			if(prop == null) {
				return new HiwiiException();
			}
			IdentifierDomain domain = new IdentifierDomain();
			domain.setName(name);
			return domain;
		} catch (DatabaseException e) {
			return new HiwiiException();
		} catch (IOException e) {
			return new HiwiiException();
		} catch (ApplicationException e) {
			return new HiwiiException();
		} catch (Exception e) {
			return new HiwiiException();
		}
//		return null;
	}

	@Override
	public HObject doFunctionCalculation(String name, List<HObject> args) {
		// TODO Auto-generated method stub
		return super.doFunctionCalculation(name, args);
	}

	@Override
	public String toString() {
		return "Domain";
	}

}
