package net.hiwii.system;

import java.util.HashMap;
import java.util.Map;

import net.hiwii.prop.Link;
import net.hiwii.view.HObject;

/**
 * 系统属性，任何对象都可以使用
 * @author a
 *
 */
public class SystemLink extends HObject {
	private static Map<String, Link> sysprops = new HashMap<String, Link>();
	
	static{
		Link idprop = new Link();
		idprop.setName("IDENTIFIER");
		idprop.setType("String");
		sysprops.put("IDENTIFIER", idprop);   //对象的唯一名字
	}
	
	public static Link getProperty(String str){
		if(sysprops.containsKey(str)) {
			return sysprops.get(str);
		}else {
			return null;
		}
	}
	
}
