package net.hiwii.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.hiwii.cognition.Expression;
import net.hiwii.cognition.result.JudgmentResult;
import net.hiwii.collection.EnumSet;
import net.hiwii.def.Definition;
import net.hiwii.def.ExceptionClass;
import net.hiwii.def.SimpleDefinition;
import net.hiwii.def.list.ListClass;
import net.hiwii.def.list.SetClass;
import net.hiwii.def.list.Tuple;
import net.hiwii.message.HiwiiException;
import net.hiwii.system.obj.AbstractDomain;
import net.hiwii.system.util.StringUtil;
import net.hiwii.user.GroupDefinition;
import net.hiwii.user.UserDefinition;
import net.hiwii.view.HObject;

/**
 * Localhost是一个特殊对象。所以定义并不是localhost的子定义。
 * 所以定义都可以认为是localhost的组成部分。
 * 20160520before
 * 系统定义的几个最上层已定义
 * level 0:L for LocalHost
 * level 1:[O/C] Object/Cognition
 * level Cognition: [A|E|L|S] Abstraction|Expression|List|Set
 * 
 * @author ha-wangzhenhai
 *
 */
public class SystemDefinition {
	public static Map<String,Definition> defs =
		new HashMap<String,Definition>();
	public static Map<String,String> idname =
			new HashMap<String,String>();
	static{
		defs.put("LocalHost", new SimpleDefinition("LocalHost","L"));
		defs.put("Object", new SimpleDefinition("Object","L.O"));//继承Operation
		defs.put("Cognition", new SimpleDefinition("Cognition","L.C"));//继承Operation
		defs.put("Exception", new ExceptionClass("Exception","L.X"));//继承Operation
		
		defs.put("Array", new ListClass()); //SimpleDefinition("Array", "Array")
		defs.put("List", new ListClass());
		defs.put("Map", new SimpleDefinition("Map","Map"));
		defs.put("Set", new SetClass());
		defs.put("Tuple", new Tuple());
		defs.put("Expression", new SimpleDefinition("Expression","L.E"));
		
		defs.put("Action", new SimpleDefinition("Action","L.A"));
		defs.put("Judgment", new SimpleDefinition("Judgment","L.J"));//status
		
		defs.put("Domain", new AbstractDomain());
		
		defs.put("Time", new SimpleDefinition("Time","L.O.t"));
		
		defs.put("month", new SimpleDefinition("month","L.O.t.m"));
		defs.put("day", new SimpleDefinition("day","L.O.t.d"));
		defs.put("hour", new SimpleDefinition("hour","L.O.t.h"));
		defs.put("minute", new SimpleDefinition("minute","L.O.t.mi"));
		defs.put("second", new SimpleDefinition("second","L.O.t.s"));
		defs.put("milliSecond", new SimpleDefinition("second","L.O.t.milli"));
		
		defs.put("Round", new SimpleDefinition("Round","L.C.R"));
		
		defs.put("String", new SimpleDefinition("String","L.O.E.S"));    //"L.C.E.S"
		defs.put("Number", new SimpleDefinition("Number","L.O.E.N"));  //"L.C.E.N"
//		defs.put("Character", "L.C.E.C"); character is regular expression,using "c" instead of 'c'
		
		defs.put("Integer", new SimpleDefinition("Integer","L.O.E.N.I"));
		defs.put("Float", new SimpleDefinition("Float","L.O.E.N.F"));
		
		defs.put("User", new UserDefinition());
		defs.put("Group", new GroupDefinition());
		defs.put("Role", new SimpleDefinition("Role","Role"));
		
//		defs.put("Array", new SimpleDefinition("Array", "Array"));
//		defs.put("List", new SimpleDefinition("List", "List"));
//		defs.put("Map", new SimpleDefinition("Map","Map"));
		
//		defacts.put("L.C.Se|enum", "");


	}
	
	public static Definition getDefinition(String name) {
		if(contains(name)) {
			return defs.get(name);
		}else {
			return null;
		}
	}
	public static boolean contains(String name){
		return defs.containsKey(name);
	}
	
	
	/**
	 * sig这里指定义名
	 * @param sig
	 * @param name
	 * @param args
	 * @return
	 */
	public static HObject doFunctionOperation(String sig, String name, List<HObject> args){
		if(sig.equals("Set")){
			if(name.equals("enum")){
				//EntityUtil.hasDuplicate(args)确保没有重复 if(yes) exception
				EnumSet es = new EnumSet();
				es.setItems(args);
				return es;
			}
		}
		return null;
	}
	
	public static Expression doIsPositive(String name1, String name2){
		String sig1,sig2;
		if(defs.containsKey(name1)){
			sig1 = defs.get(name1).getSignature();
		}else{
			return new HiwiiException();
		}
		if(defs.containsKey(name2)){
			sig2 = defs.get(name2).getSignature();
		}else{
			return new HiwiiException();
		}
		JudgmentResult jr = new JudgmentResult();
		if(StringUtil.matched(sig1, sig2)){
			jr.setResult(true);			
		}else{
			jr.setResult(false);	
		}
		return jr;
	}
}
