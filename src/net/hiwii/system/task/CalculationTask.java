package net.hiwii.system.task;

import java.util.List;

import net.hiwii.cognition.Expression;
import net.hiwii.cognition.result.BreakReturn;
import net.hiwii.cognition.result.ExitEnd;
import net.hiwii.cognition.result.JudgmentResult;
import net.hiwii.cognition.result.ReturnEnd;
import net.hiwii.cognition.result.ReturnResult;
import net.hiwii.cognition.result.SkipReturn;
import net.hiwii.context.RuntimeContext;
import net.hiwii.message.HiwiiException;

public class CalculationTask extends Task {
	private Expression result;
	public Expression getResult() {
		return result;
	}
	public void setResult(Expression result) {
		this.result = result;
	}
	@Override
	public void run() {
		List<Expression> list = getProgram().getArray();
		for(Expression expr:list) {
			getContext().doCalculation(expr);
		}
		Expression result = null;
		RuntimeContext rc = getContext();
		for(Expression expr:list){
			//只允许context操作
//			result = rc.doSilentAction(expr);
			result = rc.doAction(expr);
			
			if(result instanceof SkipReturn){
				break; //consume skip identifier
			}else if(result instanceof BreakReturn){
				this.result = result;
			}else if(result instanceof ExitEnd){
				this.result = result;
			}else if(result instanceof ReturnEnd){
				this.result = result;
			}else if(result instanceof JudgmentResult){
				this.result = result;
			}else if(result instanceof ReturnResult){
				this.result = result;
			}

			if(result instanceof HiwiiException){
				this.result = result;
			}
		}
		this.result = new HiwiiException();
	}
	
}
