package net.hiwii.system.task;

import net.hiwii.context.RuntimeContext;
import net.hiwii.expr.BraceExpression;
import net.hiwii.message.HiwiiException;

/**
 * task表达式都是指多语句的程序表达式。
 * @author hiwii
 * LastUpdate 2022年2月1日
 */
public class Task extends Thread{
	private RuntimeContext context;
	private String key;
	private int debug; //debug=1,debug status
	private BraceExpression program;
	private HiwiiException exception;
	private boolean success;
	private int line;
	
	public RuntimeContext getContext() {
		return context;
	}
	public void setContext(RuntimeContext context) {
		this.context = context;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}

	public int getLine() {
		return line;
	}
	public void setLine(int line) {
		this.line = line;
	}
	public BraceExpression getProgram() {
		return program;
	}
	public void setProgram(BraceExpression program) {
		this.program = program;
	}
	public HiwiiException getException() {
		return exception;
	}
	public void setException(HiwiiException exception) {
		this.exception = exception;
	}
	public int getDebug() {
		return debug;
	}
	public void setDebug(int debug) {
		this.debug = debug;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	
//	public String getState() {
//		return state;
//	}
//	public void setState(String state) {
//		this.state = state;
//	}
	
//	public int start() {
//		return -1;
//	}
//	
//	public int stop() {
//		return -1;
//	}
//	
//	public int pause() {
//		return -1;
//	}
//	
//	public int step() {
//		return -1;
//	}
	
}
