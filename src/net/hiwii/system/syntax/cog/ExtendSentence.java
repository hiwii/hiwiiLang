package net.hiwii.system.syntax.cog;


public class ExtendSentence extends CognitionAction {
	private String parent;//父定义名
	private String name;
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
