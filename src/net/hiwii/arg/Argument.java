package net.hiwii.arg;

import java.io.IOException;
import java.util.List;

import com.sleepycat.je.DatabaseException;

import net.hiwii.cognition.Expression;
import net.hiwii.system.exception.ApplicationException;
import net.hiwii.view.HObject;

public class Argument extends HObject {
	private String name;
	private String type;
	private List<Expression> limits;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<Expression> getLimits() {
		return limits;
	}
	public void setLimits(List<Expression> limits) {
		this.limits = limits;
	}
	public boolean doAccept(HObject ent) 
			throws DatabaseException, IOException, ApplicationException, Exception{
		return true;
	}
}
