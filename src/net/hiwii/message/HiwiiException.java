package net.hiwii.message;

import net.hiwii.cognition.Expression;

public class HiwiiException extends Expression {
	private String name;
	private String code;
	private String message;
	private HiwiiException parentMessage;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public HiwiiException() {
		super();
		this.name = "Exception";
	}
	
	public HiwiiException(String message) {
		this.message = message;
	}

	public HiwiiException(String name, String message) {
//		super(name, message);
		// TODO Auto-generated constructor stub
	}

	public HiwiiException getParentMessage() {
		return parentMessage;
	}
	public void setParentMessage(HiwiiException parentMessage) {
		this.parentMessage = parentMessage;
	}
	@Override
	public String toString() {
		if(message == null){
			if(code != null) {
				return code;
			}
			return "Exception happened!";
		}else{
			if(code != null) {
				return code + ":" + message;
			}
			return message;
		}
	}

}
