package net.hiwii.prop;

import net.hiwii.view.HObject;

public class PropertyValue extends HObject {
	private Link prop;
	private HObject value;
	
	public Link getProp() {
		return prop;
	}
	public void setProp(Link prop) {
		this.prop = prop;
	}
	public HObject getValue() {
		return value;
	}
	public void setValue(HObject value) {
		this.value = value;
	}
}
