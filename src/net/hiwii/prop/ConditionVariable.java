package net.hiwii.prop;

import java.util.List;

import net.hiwii.cognition.Expression;

public class ConditionVariable extends Variable {
	private List<Expression> conditions;

	public List<Expression> getConditions() {
		return conditions;
	}

	public void setConditions(List<Expression> conditions) {
		this.conditions = conditions;
	}
}
