package net.hiwii.prop;

import net.hiwii.view.HObject;

/**
 * variable是property的系统属性。
 * property的定义和赋值分离。而variable统一在一起。
 * @author hiwii
 *
 */
public class Variable extends Link {
	private HObject value;
	
	public HObject getValue() {
		return value;
	}
	public void setValue(HObject value) {
		this.value = value;
	}
}
