package net.hiwii.db.key;

import java.io.UnsupportedEncodingException;

import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.SecondaryDatabase;
import com.sleepycat.je.SecondaryKeyCreator;

public class MultiHashKeyCreator  implements SecondaryKeyCreator{
	@Override
	public boolean createSecondaryKey(SecondaryDatabase arg0, DatabaseEntry key, DatabaseEntry data,
			DatabaseEntry result) {
		try {
			String str = new String(key.getData(), "UTF-8");
			int start = str.indexOf('%');
			if(start > 0) {
				start++;
			}else {
				return false;
			}
			int pos = str.indexOf('-');
			if(pos > 0){
				result.setData(str.substring(start + 1, pos).getBytes("UTF-8"));
				return true;
			}else{
				result.setData(str.substring(start + 1).getBytes("UTF-8"));
				return true;
			}
		} catch (UnsupportedEncodingException e) {
			return false;
		}
	}

}
