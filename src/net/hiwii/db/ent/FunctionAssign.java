package net.hiwii.db.ent;

import java.util.List;

import net.hiwii.view.HObject;

public class FunctionAssign {
	private String name;
	private HObject value;
	private List<HObject> arguments;
	private String type;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public HObject getValue() {
		return value;
	}
	public void setValue(HObject value) {
		this.value = value;
	}
	public List<HObject> getArguments() {
		return arguments;
	}
	public void setArguments(List<HObject> arguments) {
		this.arguments = arguments;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
