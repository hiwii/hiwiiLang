package net.hiwii.db.ent;

import java.util.List;

import net.hiwii.arg.Argument;

public class MappingFormat {
	private String name;
	private List<Argument> arguments;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Argument> getArguments() {
		return arguments;
	}
	public void setArguments(List<Argument> arguments) {
		this.arguments = arguments;
	}
	
		
}
