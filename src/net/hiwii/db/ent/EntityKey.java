package net.hiwii.db.ent;

import net.hiwii.view.HObject;

/**
 * FunctionAssign以hash存储，存在碰撞现象
 * key = name + "#" + args.size() + "%" + hash + "." + nounce;
 * nounce是随机整数
 * 
 * 该类用于发现functionAssign。
 * 当发现found=true，target存储值，key=发现记录key
 * 当未发现，found=false，target=null，key=newKey
 * @author hiwii
 *
 */
public class EntityKey {
	private HObject target;
	private String key;
	private boolean found;
//	private int nounce;
	
	public HObject getTarget() {
		return target;
	}
	public void setTarget(HObject target) {
		this.target = target;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public boolean isFound() {
		return found;
	}
	public void setFound(boolean found) {
		this.found = found;
	}
//	public int getNounce() {
//		return nounce;
//	}
//	public void setNounce(int nounce) {
//		this.nounce = nounce;
//	}
}	
