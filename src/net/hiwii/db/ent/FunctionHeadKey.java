package net.hiwii.db.ent;

public class FunctionHeadKey extends FunctionHead {
	private String key;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}
