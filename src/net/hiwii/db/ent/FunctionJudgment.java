package net.hiwii.db.ent;

import java.util.List;

import net.hiwii.view.HObject;

public class FunctionJudgment {

	private String name;
	private boolean positive;
	private List<HObject> arguments;
	private String type;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public boolean isPositive() {
		return positive;
	}
	public void setPositive(boolean positive) {
		this.positive = positive;
	}
	public List<HObject> getArguments() {
		return arguments;
	}
	public void setArguments(List<HObject> arguments) {
		this.arguments = arguments;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	

}
