package net.hiwii.db.bind;

import com.sleepycat.bind.tuple.TupleBinding;
import com.sleepycat.bind.tuple.TupleInput;
import com.sleepycat.bind.tuple.TupleOutput;

import net.hiwii.state.IdStatement;

public class IdStatementBinding extends TupleBinding<IdStatement> {

	@Override
	public IdStatement entryToObject(TupleInput arg0) {
		IdStatement stm = new IdStatement();
		stm.setName(arg0.readString());
//		stm.setProgress(arg0.readString());
//		stm.setHappen(arg0.readString());
		return stm;
	}

	@Override
	public void objectToEntry(IdStatement arg0, TupleOutput arg1) {
		arg1.writeString(arg0.getName());
//		arg1.writeString(arg0.getProgress());
//		arg1.writeString(arg0.getHappen());
		
	}

}
