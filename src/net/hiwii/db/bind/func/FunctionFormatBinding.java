package net.hiwii.db.bind.func;

import java.util.ArrayList;
import java.util.List;

import com.sleepycat.bind.tuple.TupleBinding;
import com.sleepycat.bind.tuple.TupleInput;
import com.sleepycat.bind.tuple.TupleOutput;

import net.hiwii.arg.Argument;
import net.hiwii.db.ent.FunctionFormat;

public class FunctionFormatBinding extends TupleBinding<FunctionFormat> {
	@Override
	public FunctionFormat entryToObject(TupleInput arg0) {
		FunctionFormat ff = new FunctionFormat();
		String name = arg0.readString();
		
		int num = arg0.readInt();
//		Map<String, String> map = new HashMap<String, String>();
		List<Argument> arguments = new ArrayList<Argument>();
		for(int n=0;n<num;n++){
			Argument arg = new Argument();
			String argname = arg0.readString();
			String type = arg0.readString();
			arg.setName(argname);
			arg.setType(type);
			arguments.add(arg);
		}
		ff.setName(name);
		ff.setArguments(arguments);
		return ff;
	}

	@Override
	public void objectToEntry(FunctionFormat arg0, TupleOutput arg1) {
		arg1.writeString(arg0.getName());
		arg1.writeInt(arg0.getArguments().size());
		
		for (Argument arg: arg0.getArguments()) {
			arg1.writeString(arg.getName());
			arg1.writeString(arg.getType());
		}
	}

}
