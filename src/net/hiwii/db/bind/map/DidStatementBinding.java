package net.hiwii.db.bind.map;

import java.util.ArrayList;
import java.util.List;

import com.sleepycat.bind.tuple.TupleBinding;
import com.sleepycat.bind.tuple.TupleInput;
import com.sleepycat.bind.tuple.TupleOutput;

import net.hiwii.cognition.Expression;
import net.hiwii.state.DidStatement;
import net.hiwii.system.util.StringUtil;

public class DidStatementBinding extends TupleBinding<DidStatement> {

	@Override
	public DidStatement entryToObject(TupleInput arg0) {
		DidStatement stm = new DidStatement();
		
		stm.setPositive(arg0.readBoolean());
		stm.setVerb(StringUtil.parseString(arg0.readString()));
		int n = arg0.readInt();
		if(n == 0) {
			stm.setAdverb(null);
			return stm;
		}
		List<Expression> list = new ArrayList<Expression>();
		for(;n>0;n--) {
			Expression expr = StringUtil.parseString(arg0.readString());
			list.add(expr);
		}
		stm.setAdverb(list);
		return stm;
	}

	@Override
	public void objectToEntry(DidStatement arg0, TupleOutput arg1) {
		arg1.writeBoolean(arg0.isPositive());
		arg1.writeString(arg0.getVerb().toString());
		int n = 0;
		if(arg0.getAdverb() != null) {
			n = arg0.getAdverb().size();
		}
		arg1.writeInt(n);
		for(int i = 0;i<n;i++) {
			arg1.writeString(arg0.getAdverb().get(i).toString());
		}
	}

}
