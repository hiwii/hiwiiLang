package net.hiwii.db.bind.map;

import com.sleepycat.bind.tuple.TupleBinding;
import com.sleepycat.bind.tuple.TupleInput;
import com.sleepycat.bind.tuple.TupleOutput;

import net.hiwii.state.HasStatement;
import net.hiwii.system.util.StringUtil;

public class HasStatementBinding extends TupleBinding<HasStatement> {

	@Override
	public HasStatement entryToObject(TupleInput arg0) {
		HasStatement stm = new HasStatement();
//		stm.setName(arg0.readString());
		
		stm.setPositive(arg0.readBoolean());
		stm.setNumber(StringUtil.parseString(arg0.readString()));
		stm.setTarget(StringUtil.parseString(arg0.readString()));
		return stm;
	}

	@Override
	public void objectToEntry(HasStatement arg0, TupleOutput arg1) {
		arg1.writeBoolean(arg0.isPositive());
		arg1.writeString(arg0.getNumber().toString());
		arg1.writeString(arg0.getTarget().toString());
	}

}
