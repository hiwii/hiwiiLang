package net.hiwii.db.bind;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.sleepycat.bind.tuple.TupleBinding;
import com.sleepycat.bind.tuple.TupleInput;
import com.sleepycat.bind.tuple.TupleOutput;
import com.sleepycat.je.DatabaseException;

import net.hiwii.db.ent.StoredValue;
import net.hiwii.state.FunctionStatement;
import net.hiwii.system.exception.ApplicationException;
import net.hiwii.system.util.EntityUtil;
import net.hiwii.view.HObject;

public class FunctionStatementBinding extends TupleBinding<FunctionStatement> {

	@Override
	public FunctionStatement entryToObject(TupleInput arg0) {
		FunctionStatement stm = new FunctionStatement();
		try {			
			stm.setName(arg0.readString());
			int num = arg0.readInt();
			List<HObject> list = new ArrayList<HObject>();
			for(int n=0;n<num;n++){
				StoredValue sv = new StoredValue();
				String type = arg0.readString();
				String val = arg0.readString();
				sv.setType(type);
				sv.setValue(val);
				HObject ent = EntityUtil.recordToEntity(sv);
				list.add(ent);
			}
			stm.setArguments(list);
			stm.setPositive(arg0.readBoolean());
		} catch (IndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return stm;
	}

	@Override
	public void objectToEntry(FunctionStatement arg0, TupleOutput arg1) {
		try {
			arg1.writeString(arg0.getName());
			arg1.writeInt(arg0.getArguments().size());
			for(int i=0;i<arg0.getArguments().size();i++) {
				StoredValue sv = EntityUtil.entityToRecord(arg0.getArguments().get(i));
				arg1.writeString(sv.getType());
				arg1.writeString(sv.getValue());
			}
			arg1.writeBoolean(arg0.isPositive());
		} catch (ApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
