package net.hiwii.db.bind.sub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.sleepycat.bind.tuple.TupleBinding;
import com.sleepycat.bind.tuple.TupleInput;
import com.sleepycat.bind.tuple.TupleOutput;
import com.sleepycat.je.DatabaseException;

import net.hiwii.cognition.Expression;
import net.hiwii.db.ent.StoredValue;
import net.hiwii.state.sub.SubjectMappingStatement;
import net.hiwii.system.exception.ApplicationException;
import net.hiwii.system.util.EntityUtil;
import net.hiwii.system.util.StringUtil;
import net.hiwii.view.HObject;

public class SubjectMappingStatementBinding extends TupleBinding<SubjectMappingStatement> {

	@Override
	public SubjectMappingStatement entryToObject(TupleInput arg0) {
		SubjectMappingStatement ff = new SubjectMappingStatement();
		
		try {
			String type = arg0.readString();
			String val = arg0.readString();
			StoredValue sv0 = new StoredValue();
			sv0.setType(type);
			sv0.setValue(val);
			HObject value = EntityUtil.recordToEntity(sv0);
			ff.setSubject(value);
			
			String name = arg0.readString();
			ff.setName(name);
			boolean pos = arg0.readBoolean();
			ff.setPositive(pos);
			
			int num = arg0.readInt();
			List<Expression> list = new ArrayList<Expression>();
			for(int n=0;n<num;n++){
				String str = arg0.readString();
				Expression exp = StringUtil.parseString(str);
				list.add(exp);
			}
			ff.setArguments(list);
			return ff;
		} catch (IndexOutOfBoundsException e) {
			return null;
		} catch (IllegalArgumentException e) {
			return null;
		} catch (DatabaseException e) {
			return null;
		} catch (IOException e) {
			return null;
		} catch (ApplicationException e) {
			return null;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void objectToEntry(SubjectMappingStatement arg0, TupleOutput arg1) {
		try {
			StoredValue val = EntityUtil.entityToRecord(arg0.getSubject());
			arg1.writeString(val.getType());
			arg1.writeString(val.getValue());
			
			arg1.writeString(arg0.getName());
			arg1.writeBoolean(arg0.isPositive());
			List<Expression> list = arg0.getArguments();
			arg1.writeInt(list.size());
			for(int i=0;i<arg0.getArguments().size();i++) {
				Expression expr = list.get(i);
				arg1.writeString(expr.toString());
			}
		} catch (ApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
