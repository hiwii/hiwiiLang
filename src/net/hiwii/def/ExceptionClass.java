package net.hiwii.def;

import java.util.List;

import net.hiwii.cognition.result.ExceptionObject;
import net.hiwii.expr.StringExpression;
import net.hiwii.message.HiwiiException;
import net.hiwii.view.HObject;

public class ExceptionClass extends Definition {
	
	public ExceptionClass(String name, String sign) {
		super();
		this.setName(name);
		this.setClassName(name);
		this.setSignature(sign);
	}
	@Override
	public HObject doIdentifierCalculation(String name) {
		if(name.equals("new")){
			HiwiiException except = new HiwiiException();
			except.setName(getClassName());
			ExceptionObject eo = new ExceptionObject();
			eo.setException(except);
			return eo;
		}
		return null;
	}
	@Override
	public HObject doFunctionCalculation(String name, List<HObject> args) {
		if(name.equals("new")){
			if(args.size() == 1) {
				if(!(args.get(0) instanceof StringExpression)) {
					return new HiwiiException();
				}
				StringExpression msg = (StringExpression) args.get(0);
				HiwiiException except = new HiwiiException();
				except.setName(getClassName());
				except.setMessage(msg.getValue());
				ExceptionObject eo = new ExceptionObject();
				eo.setException(except);
				return eo;
			}else if(args.size() == 2) {
				if(!(args.get(0) instanceof StringExpression)) {
					return new HiwiiException();
				}
				if(!(args.get(1) instanceof StringExpression)) {
					return new HiwiiException();
				}
				StringExpression code = (StringExpression) args.get(0);
				StringExpression msg = (StringExpression) args.get(1);
				HiwiiException except = new HiwiiException();
				except.setName(getClassName());
				except.setMessage(msg.getValue());
				except.setCode(code.getValue());
				ExceptionObject eo = new ExceptionObject();
				eo.setException(except);
				return eo;
			}
		}
		return null;
	}
	@Override
	public String toString() {
		String ret = getClassName();
		return ret;
	}
}
