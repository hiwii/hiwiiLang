package net.hiwii.def.decl;

import net.hiwii.cognition.Expression;
import net.hiwii.db.ent.FunctionFormat;

public class FunctionDeclaration extends FunctionFormat{
	private Expression statement;

	public Expression getStatement() {
		return statement;
	}

	public void setStatement(Expression statement) {
		this.statement = statement;
	}

}
