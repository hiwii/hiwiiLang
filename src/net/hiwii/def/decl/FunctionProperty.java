package net.hiwii.def.decl;

import java.util.List;

import net.hiwii.prop.Link;
import net.hiwii.view.HObject;

public class FunctionProperty extends Link {
	private List<HObject> arguments;

	public List<HObject> getArguments() {
		return arguments;
	}

	public void setArguments(List<HObject> arguments) {
		this.arguments = arguments;
	}
}
