package net.hiwii.def;

import java.util.List;

import net.hiwii.view.HObject;

public class FunctionAssignment extends Assignment {
	private List<HObject> arguments;

	public List<HObject> getArguments() {
		return arguments;
	}

	public void setArguments(List<HObject> arguments) {
		this.arguments = arguments;
	}
}
