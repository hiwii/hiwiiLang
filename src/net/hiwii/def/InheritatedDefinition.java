package net.hiwii.def;

public class InheritatedDefinition extends Definition {
	private Definition inherit;

	public Definition getInherit() {
		return inherit;
	}

	public void setInherit(Definition inherit) {
		this.inherit = inherit;
	}
}
