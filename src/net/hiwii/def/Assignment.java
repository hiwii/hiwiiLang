package net.hiwii.def;

import net.hiwii.view.HObject;

/**
 * from:hosted Object
 * @author Administrator
 *
 */
public class Assignment extends HObject {
	private String name;
	private HObject value;
//	private Property prop;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public HObject getValue() {
		return value;
	}
	public void setValue(HObject value) {
		this.value = value;
	}
}
