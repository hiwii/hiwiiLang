package net.hiwii.def.cog;

import net.hiwii.def.Judgment;
import net.hiwii.view.HObject;

public class JudgmentSubject extends Judgment {
	private HObject subject;
	
	public HObject getSubject() {
		return subject;
	}
	public void setSubject(HObject subject) {
		this.subject = subject;
	}
}
