package net.hiwii.def;

import net.hiwii.cognition.Expression;
import net.hiwii.view.HObject;


public class Cognition extends HObject{
	private Expression left;
	private HObject value;
	public Expression getLeft() {
		return left;
	}
	public void setLeft(Expression left) {
		this.left = left;
	}
	public HObject getValue() {
		return value;
	}
	public void setValue(HObject value) {
		this.value = value;
	}
}
