package net.hiwii.struct;

import java.util.List;

import net.hiwii.def.Definition;
import net.hiwii.view.HObject;

public class EntityDefinition extends HObject {
	private List<HObject> values;
	private Definition type;//部件
	private boolean countable;
	private int count;
	public List<HObject> getValues() {
		return values;
	}
	public void setValues(List<HObject> values) {
		this.values = values;
	}
	public Definition getType() {
		return type;
	}
	public void setType(Definition type) {
		this.type = type;
	}
	public boolean isCountable() {
		return countable;
	}
	public void setCountable(boolean countable) {
		this.countable = countable;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
}
