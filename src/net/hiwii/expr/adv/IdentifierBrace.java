package net.hiwii.expr.adv;

import java.util.List;

import net.hiwii.cognition.Expression;
import net.hiwii.expr.IdentifierExpression;
import net.hiwii.expr.sent.ConditionExpression;

public class IdentifierBrace extends IdentifierExpression implements Adverb{
	private List<Expression> statements;
	
	public IdentifierBrace() {
		
	}

	@Override
	public String toString() {
		String str = getName();
		str = str + "{";
		int i = getStatements().size() - 1;
		
		for(Expression exp:getStatements()){
			if(i != 0 ) {
				str = str + exp.toString()  + ",";
			}else {
				str = str + exp.toString();
			}
			i--;
		}
		str = str + "}";
		return str;
	}

	@Override
	public List<Expression> getStatements() {
		return statements;
	}

	@Override
	public void setStatements(List<Expression> adv) {
		this.statements = adv;
	}

	@Override
	public Expression getContent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setContent(Expression content) {
		// TODO Auto-generated method stub
		
	}
}
