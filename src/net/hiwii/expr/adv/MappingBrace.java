package net.hiwii.expr.adv;

import java.util.List;

import net.hiwii.cognition.Expression;
import net.hiwii.expr.MappingExpression;

public class MappingBrace extends Expression implements Adverb{
	private String name;
	private List<Expression> arguments;
	private List<Expression> statements;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Expression> getArguments() {
		return arguments;
	}
	public void setArguments(List<Expression> arguments) {
		this.arguments = arguments;
	}
	@Override
	public List<Expression> getStatements() {
		return statements;
	}
	@Override
	public void setStatements(List<Expression> statements) {
		this.statements = statements;
	}
	
	@Override
	public void setContent(Expression content) {
		MappingExpression fe = (MappingExpression) content;
		this.setName(fe.getName());
		this.setArguments(fe.getArguments());
	}
	
	@Override
	public Expression getContent() {
		// TODO Auto-generated method stub
		return null;
	}
}
