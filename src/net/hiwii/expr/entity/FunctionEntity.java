package net.hiwii.expr.entity;

import java.util.List;

import net.hiwii.cognition.Expression;
import net.hiwii.view.HObject;

public class FunctionEntity extends Expression {
	private String name;
	private List<HObject> arguments;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<HObject> getArguments() {
		return arguments;
	}
	public void setArguments(List<HObject> arguments) {
		this.arguments = arguments;
	}
	
}
