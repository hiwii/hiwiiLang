package net.hiwii.expr;

import java.util.List;

import net.hiwii.view.HObject;

public class FunctionLambda extends FunctionExpression {
	private List<HObject> values;
	
	public List<HObject> getValues() {
		return values;
	}
	public void setValues(List<HObject> values) {
		this.values = values;
	}
}
