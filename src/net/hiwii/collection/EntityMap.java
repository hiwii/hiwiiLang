package net.hiwii.collection;

import java.util.HashMap;
import java.util.Map;

import net.hiwii.view.HObject;

public class EntityMap extends HObject {
	private Map<String, HObject> values;
	
	public EntityMap() {
		super();
		values = new HashMap<String, HObject>();
	}

	public Map<String, HObject> getValues() {
		return values;
	}

	public void setValues(Map<String, HObject> values) {
		this.values = values;
	}

	public void put(String key, HObject value) {
		if(values == null) {
			values = new HashMap<String, HObject>();
		}
		values.put(key, value);
	}
	@Override
	public String toString() {
		String ret = "{";
		int i = values.size();
		for(Map.Entry<String, HObject> ent:values.entrySet()){
			if(i > 1) {
				ret = ent.getKey() + ":" + ent.getValue().toString() + ",";
			}else {
				ret = ent.getKey() + ":" + ent.getValue().toString();
			}
		}
		
		ret = ret + "}";
		return ret;
	}
}
