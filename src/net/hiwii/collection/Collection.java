package net.hiwii.collection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.sleepycat.je.DatabaseException;

import net.hiwii.def.TypeView;
import net.hiwii.system.exception.ApplicationException;
import net.hiwii.view.HObject;

public class Collection extends HObject {
	private List<HObject> items;

	public Collection(){
		items = new ArrayList<HObject>();
	}
	public Collection(List<HObject> items){
		this.items = items;
	}
	public List<HObject> getItems() {
		return items;
	}

	public void setItems(List<HObject> items) {
		this.items = items;
	}
	
	public boolean typeDecision(TypeView tv) 
			throws DatabaseException, IOException, ApplicationException, Exception{
		for(HObject ent:items){
			boolean jdg = tv.doAccept(ent);
			if(!jdg){
				return false;
			}
		}
		return true;
	}
}
